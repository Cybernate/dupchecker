﻿using System;

namespace DupChecker
{
	internal class SearchResult
	{
		public bool Found
		{
			get;
			protected set;
		}

		public int? Value
		{
			get;
			protected set;
		}

		public SearchResult(bool found, int? value)
		{
			Found = found;
			Value = value;
		}
	}

	public class NeedleFinder
	{
		public NeedleFinder()
		{
		}

	
		public int? Find(int[] input)
		{
			if (input == null || input.Length < 1)
			{
				throw new Exception("Input list cannot be empty.");
			}
			var min = 1;
			var max = input.Length;
			var result = PerformBinarySearch(input, min, max);
			return result.Value;
		}

		private static int FindMidPoint(int min, int max)
		{
			int midPoint = (max + min);
			midPoint = (midPoint % 2 == 0) ? midPoint / 2 : (int)Math.Floor(midPoint / 2.0);

			return midPoint;
		}

		private SearchResult PerformBinarySearch(int[] input, int min, int max)
		{
			if (input.Length < 2)
			{
				return new SearchResult(false, null);
			}

			else if (input.Length == 2)
			{
				if (input[0] == input[1])
				{
					return new SearchResult(true, input[0]);
				}
				return new SearchResult(false, null);
			}

			int midPoint = FindMidPoint(min, max);
			var range = midPoint - min + 1;

			int occurances = CheckOccurances(input, min, midPoint);
			if (occurances == 0)
			{
				return new SearchResult(false, null);
			}

			if (occurances > range)
			{
				if (range <= 1)
				{
					return new SearchResult(true, min);
				}
				return PerformBinarySearch(input, min, midPoint);
			}
			else
			{
				return PerformBinarySearch(input, midPoint + 1, max);
			}
		}

		private int CheckOccurances(int[] input, int start, int end)
		{
			int count = 0;
			for (var j = 0; j < input.Length; j++)
			{
				var item = input[j];
				if (item >= start && item <= end)
				{
					count++;
				}
			}
			return count;
		}
	}
}
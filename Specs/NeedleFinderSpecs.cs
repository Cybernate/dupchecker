﻿using Machine.Specifications;
using System;

namespace DupChecker.Specs
{
	public class NeedleFinderSpecs
	{
		protected static NeedleFinder _finder;
		protected static int[] _inputs;
		protected static int? returnValue;

		[Subject(typeof(NeedleFinder))]
		public class When_the_input_list_is_empty
		{
			protected static Exception _exception;

			private Establish context = () =>
			{
				_finder = new NeedleFinder();
				_inputs = new int[] { };
			};

			private Because of = () => _exception = Catch.Exception(() => _finder.Find(_inputs));

			private It should_fail = () => _exception.ShouldNotBeNull();
			private It should_fail_with_proper_message = () => _exception.Message.ShouldEqual("Input list cannot be empty.");
		}

		public class When_the_input_list_is_null
		{
			protected static Exception _exception;

			private Establish context = () =>
			{
				_finder = new NeedleFinder();
				_inputs = null;
			};

			private Because of = () => _exception = Catch.Exception(() => _finder.Find(_inputs));

			private It should_fail = () => _exception.ShouldNotBeNull();
			private It should_fail_with_proper_message = () => _exception.Message.ShouldEqual("Input list cannot be empty.");
		}

		public class When_input_contains_no_duplicates
		{
			private Establish context = () =>
			{
				_finder = new NeedleFinder();
				_inputs = new int[] { 1, 2, 3 };
			};

			private Because of = () => returnValue = _finder.Find(_inputs);
			private It should_return_null = () => returnValue.ShouldBeNull();
		}

		public class When_input_contains_one_duplicate
		{
			private Establish context = () =>
			{
				_finder = new NeedleFinder();
				_inputs = new int[] { 1, 2, 3, 2 };
			};

			private Because of = () => returnValue = _finder.Find(_inputs);
			private It should_return_duplicate_number = () => returnValue.ShouldEqual(2);
		}

		public class When_input_contains_just_a_duplicate_pair
		{
			private Establish context = () =>
			{
				_finder = new NeedleFinder();
				_inputs = new int[] { 2, 2 };
			};

			private Because of = () => returnValue = _finder.Find(_inputs);
			private It should_return_duplicate_number = () => returnValue.ShouldEqual(2);
		}

		public class When_input_is_of_odd_numbered_length_and_has_duplicates
		{
			private Establish context = () =>
			{
				_finder = new NeedleFinder();
				_inputs = new int[] { 1, 2, 2 };
			};

			private Because of = () => returnValue = _finder.Find(_inputs);
			private It should_return_duplicate_number = () => returnValue.ShouldEqual(2);
		}

		public class When_input_is_of_even_numbered_length_and_has_duplicates
		{
			private Establish context = () =>
			{
				_finder = new NeedleFinder();
				_inputs = new int[] { 1, 2, 2, 3 };
			};

			private Because of = () => returnValue = _finder.Find(_inputs);
			private It should_return_duplicate_number = () => returnValue.ShouldEqual(2);
		}

		public class When_input_has_multiple_sets_of_duplicates
		{
			private Establish context = () =>
			{
				_finder = new NeedleFinder();
				_inputs = new int[] { 1, 2, 3, 4, 2, 3,5 };
			};

			private Because of = () => returnValue = _finder.Find(_inputs);
			private It should_return_duplicate_that_has_lowest_numeric_value = () => returnValue.ShouldEqual(2);
		}
	}
}